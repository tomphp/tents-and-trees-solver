module SolverSpec where

import Test.Hspec

import Solver

boardWithNoTrees = BoardCharacteristics
  { tentsPerColumn = [0,0]
  , tentsPerRow = [0, 0]
  , trees = []
  }

boardFullOfGrass = SolvedBoard
  [ [Grass, Grass]
  , [Grass, Grass]
  ]

boardWithOneTree = BoardCharacteristics
  { tentsPerColumn = [1,0]
  , tentsPerRow = [1, 0]
  , trees = [(0,1)]
  }

solvedBoardWithOneTree = SolvedBoard
  [[Tent, Grass]
  ,[Tree, Grass]
  ]

boardWithOneTreeInMiddle = BoardCharacteristics
  { tentsPerColumn = [1,0,0]
  , tentsPerRow = [0,1,0]
  , trees = [(1,1)]
  }

boardWithOneTreeInMiddleSolved = SolvedBoard
  [
    [Grass, Grass, Grass]
   ,[Tent, Tree, Grass]
   ,[Grass, Grass, Grass]
  ]

board5x5A1 = BoardCharacteristics
  { tentsPerColumn = [2, 0, 1, 1, 1]
  , tentsPerRow = [1, 1, 0, 2, 1]
  , trees = [(1,1), (3,1), (0,3), (1,3), (4,4)]
  }

board5x5A1Solved = SolvedBoard
  [[Grass, Grass, Grass, Tent, Grass]
  ,[Tent, Tree, Grass, Tree, Grass]
  ,[Grass, Grass, Grass, Grass, Grass]
  ,[Tree, Tree, Tent, Grass, Tent]
  ,[Tent, Grass, Grass, Grass, Tree]
  ]

board5x5A2 = BoardCharacteristics
  { tentsPerColumn = [1, 1, 1, 1, 1]
  , tentsPerRow = [2, 0, 1, 0, 2]
  , trees = [(1,0), (3, 0), (1, 3), (1,4), (2, 4)]
  }

board5x5A2Solved = SolvedBoard
  [[Grass, Tree, Tent, Tree, Tent]
  ,[Grass, Grass, Grass, Grass, Grass]
  ,[Grass, Tent, Grass, Grass, Grass]
  ,[Grass, Tree, Grass, Grass, Grass]
  ,[Tent, Tree, Tree, Tent, Grass]
  ]

board6x6A1 = BoardCharacteristics
  { tentsPerColumn = [0, 2, 0, 2, 1, 2]
  , tentsPerRow = [1, 2, 1, 1, 1, 1]
  , trees = [(1,0), (4, 0), (4, 1), (5,1), (3, 2), (1, 3), (3, 5)]
  }

board6x6A1Solved = SolvedBoard
  [[Grass, Tree, Grass, Grass, Tree, Tent]
  ,[Grass, Tent, Grass, Tent, Tree, Tree]
  ,[Grass, Grass, Grass, Tree, Grass, Tent]
  ,[Grass, Tree, Grass, Tent, Grass, Grass]
  ,[Grass, Tent, Grass, Grass, Grass, Grass]
  ,[Grass, Grass, Grass, Tree, Tent, Grass]
  ]

board6x6A2 = BoardCharacteristics
  { tentsPerColumn = [2, 0, 2, 0, 2, 1]
  , tentsPerRow = [2, 1, 1, 2, 1, 0]
  , trees = [(1,0), (3, 0), (4, 1), (0,2), (2, 2), (5, 3), (0, 4)]
  }

board6x6A2Solved = SolvedBoard
  [[Grass,Tree,Tent,Tree,Tent,Grass]
  ,[Tent,Grass,Grass,Grass,Tree,Grass]
  ,[Tree,Grass,Tree,Grass,Tent,Grass]
  ,[Tent,Grass,Tent,Grass,Grass,Tree]
  ,[Tree,Grass,Grass,Grass,Grass,Tent]
  ,[Grass,Grass,Grass,Grass,Grass,Grass]
  ]

board6x6B20 = BoardCharacteristics
  { tentsPerColumn = [2, 1, 1, 1, 1, 1]
  , tentsPerRow = [1, 1, 1, 2, 0, 2]
  , trees = [(1,0), (0, 1), (2, 1), (4,2), (1, 3), (1, 4), (4, 5)]
  }

board6x6B20Solved = SolvedBoard
  [[Tent,Tree,Grass,Grass,Grass,Grass]
  ,[Tree,Grass,Tree,Tent,Grass,Grass]
  ,[Tent,Grass,Grass,Grass,Tree,Grass]
  ,[Grass,Tree,Tent,Grass,Tent,Grass]
  ,[Grass,Tree,Grass,Grass,Grass,Grass]
  ,[Grass,Tent,Grass,Grass,Tree,Tent]
  ]

board7x7B2 = BoardCharacteristics
  { tentsPerColumn = [1,2,1,1,1,1,2]
  , tentsPerRow = [1,2,2,0,1,2,1]
  , trees = [(0,0), (1,1), (4,1), (6,1), (1,3), (6,3), (1, 5), (4,5), (6,5)]
  }

board7x7B2Solved = SolvedBoard
  [[Tree,Grass,Grass,Grass,Grass,Grass,Tent]
  ,[Tent,Tree,Tent,Grass,Tree,Grass,Tree]
  ,[Grass,Grass,Grass,Grass,Tent,Grass,Tent]
  ,[Grass,Tree,Grass,Grass,Grass,Grass,Tree]
  ,[Grass,Tent,Grass,Grass,Grass,Grass,Grass]
  ,[Grass,Tree,Grass,Tent,Tree,Tent,Tree]
  ,[Grass,Tent,Grass,Grass,Grass,Grass,Grass]
  ]

board12x12A2 = BoardCharacteristics
  { tentsPerColumn = [5,1,4,2,2,2,4,0,4,1,5,1]
  , tentsPerRow = [4,1,3,2,4,1,3,3,1,4,1,4]
  , trees =
    [ (1,0), (6,0)
    , (2,1), (6,1), (8,1), (10,1)
    , (0,2)
    , (1,3), (2,3), (3,3), (6,3), (8,3), (10, 3)
    , (0,4), (9,4)
    , (2,6), (8,6), (11, 6)
    , (1,7), (4,7), (5,7), (6,7)
    , (1,8)
    , (2,9), (6,9)
    , (3,10), (9,10), (11,10)
    , (1,11), (9,11), (11,11)
    ]
  }

board12x12A2Solved = SolvedBoard
  [[Grass,Tree,Tent,Grass,Grass,Tent,Tree,Grass,Tent,Grass,Tent,Grass]
  ,[Tent,Grass,Tree,Grass,Grass,Grass,Tree,Grass,Tree,Grass,Tree,Grass]
  ,[Tree,Grass,Tent,Grass,Grass,Grass,Tent,Grass,Grass,Grass,Tent,Grass]
  ,[Tent,Tree,Tree,Tree,Tent,Grass,Tree,Grass,Tree,Grass,Tree,Grass]
  ,[Tree,Grass,Tent,Grass,Grass,Grass,Tent,Grass,Tent,Tree,Tent,Grass]
  ,[Tent,Grass,Grass,Grass,Grass,Grass,Grass,Grass,Grass,Grass,Grass,Grass]
  ,[Grass,Grass,Tree,Grass,Tent,Grass,Tent,Grass,Tree,Grass,Tent,Tree]
  ,[Tent,Tree,Tent,Grass,Tree,Tree,Tree,Grass,Tent,Grass,Grass,Grass]
  ,[Grass,Tree,Grass,Grass,Grass,Tent,Grass,Grass,Grass,Grass,Grass,Grass]
  ,[Grass,Tent,Tree,Tent,Grass,Grass,Tree,Grass,Grass,Tent,Grass,Tent]
  ,[Grass,Grass,Grass,Tree,Grass,Grass,Tent,Grass,Grass,Tree,Grass,Tree]
  ,[Tent,Tree,Grass,Tent,Grass,Grass,Grass,Grass,Tent,Tree,Tent,Tree]
  ]

spec :: Spec
spec = describe "Solver" $ do
  it "should fill board with grass when board has no trees" $
    solve boardWithNoTrees `shouldBe` boardFullOfGrass
  it "should solve board with one tree" $
    solve boardWithOneTree `shouldBe` solvedBoardWithOneTree
  it "should solve board with one tree in the middle" $
    solve boardWithOneTreeInMiddle `shouldBe` boardWithOneTreeInMiddleSolved
  it "should sovle 5x5 A 1 board" $
    solve board5x5A1 `shouldBe` board5x5A1Solved
  it "should sovle 5x5 A 2 board" $
    solve board5x5A2 `shouldBe` board5x5A2Solved
  it "should sovle 6x6 A 1 board" $
    solve board6x6A1 `shouldBe` board6x6A1Solved
  it "should sovle 6x6 A 2 board" $
    solve board6x6A2 `shouldBe` board6x6A2Solved
  it "should solve 6x6 B 20" $
    solve board6x6B20 `shouldBe` board6x6B20Solved
  it "should solve 7x7 B 1" $
    solve board7x7B2 `shouldBe` board7x7B2Solved
  it "should solve 12x12 A 2" $
    solve board12x12A2 `shouldBe` board12x12A2Solved
