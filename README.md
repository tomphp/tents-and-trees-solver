# Tents & Trees Solver

Tents & Trees is a nifty little mobile app that's kind of a cross-over between minesweeper and sudoku.  
It can be found on the [Google Playstore](https://play.google.com/store/apps/details?id=com.frozax.tentsandtrees).

### Rules:
The rules are very simple. 

* Every tree needs a tent next to it 
* Tents are not allowed to be placed diagonally to a tree
* The amounts of tents per row and column are indicated on to board
* Tents are not allowed to be placed next to each other, diagonally and normal

### Example:

<img src="https://i.imgur.com/FEi9HLB.jpg" alt="Board Unsolved" width="400"/>

<img src="https://i.imgur.com/5iZaUao.jpg" alt="Board Solved" width="400"/>
