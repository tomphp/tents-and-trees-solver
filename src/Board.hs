{-# LANGUAGE DeriveGeneric #-}

module Board
  ( Board
  , boardWidth
  , boardHeight
  , boardUnsolvedTiles
  , boardGrassTiles
  , boardTrees
  , boardTents
  , boardTentsPerColumn
  , boardTentsPerRow
  , createBoard
  , boardSolvedTiles
  , BoardCharacteristics (..)
  , Coordinate
  , Tile (..)
  ) where

import           Data.Aeson   (FromJSON, ToJSON)
import           GHC.Generics

data Tile
  = Empty
  | Grass
  | Tree
  | Tent
  deriving (Eq, Generic)

instance ToJSON Tile

instance Show Tile where
  show Empty = " "
  show Grass = "~"
  show Tree  = "T"
  show Tent  = "^"

type Coordinate = (Int, Int)

data Board = Board
  { boardWidth          :: Int
  , boardHeight         :: Int
  , boardUnsolvedTiles  :: [Coordinate]
  , boardTentsPerColumn :: [Int]
  , boardTentsPerRow    :: [Int]
  , boardSolvedTiles    :: [(Coordinate, Tile)]
  } deriving (Show, Eq)

boardGrassTiles :: Board -> [Coordinate]
boardGrassTiles = coordinatesByType Grass

boardTents :: Board -> [Coordinate]
boardTents = coordinatesByType Tent

boardTrees :: Board -> [Coordinate]
boardTrees = coordinatesByType Tree

coordinatesByType :: Tile -> Board -> [Coordinate]
coordinatesByType tileType board = map fst $ filter (\(_, tileType') -> tileType' == tileType) $ boardSolvedTiles board

data BoardCharacteristics = BoardCharacteristics
            { tentsPerColumn :: [Int]
            , tentsPerRow    :: [Int]
            , trees          :: [(Int, Int)]
            } deriving (Show, Eq, Generic)

instance FromJSON BoardCharacteristics

createBoard :: BoardCharacteristics -> Board
createBoard boardCharacteristics = Board
  { boardWidth = width
  , boardHeight = height
  , boardUnsolvedTiles = unsolvedTiles
  , boardTentsPerColumn = tentsPerColumn boardCharacteristics
  , boardTentsPerRow = tentsPerRow boardCharacteristics
  , boardSolvedTiles = map (\coordinate -> (coordinate, Tree)) $ trees boardCharacteristics
  }
  where
    unsolvedTiles = [(x,y)
                    | x <- [0..width-1]
                    , y <- [0..height-1]
                    , notElem (x,y) $ trees boardCharacteristics]
    width = length $ tentsPerColumn boardCharacteristics
    height = length $ tentsPerRow boardCharacteristics
