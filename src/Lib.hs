{-# LANGUAGE OverloadedStrings #-}

module Lib
    ( main
    ) where

import Web.Scotty
import Data.Aeson (ToJSON, FromJSON)
import Data.Text.Lazy (pack)

import Solver (BoardCharacteristics, solve)

main :: IO ()
main = scotty 3000 $ do
  get "/" $ do
    html $ "<h1> Welcome to the Tents & Trees Solver </h1>"
  post "/solve" $ do
    test <- jsonData :: ActionM BoardCharacteristics
    json $ solve test
