{-# LANGUAGE DeriveGeneric #-}

module Solver
  ( solve
  , BoardCharacteristics (..)
  ,SolvedBoard(..)
  , Tile (..)
  ) where

import           Control.Applicative
import           Data.Aeson          (FromJSON, ToJSON)
import           Data.List           (find)
import           Data.Maybe          (fromMaybe)
import           Data.Monoid
import           GHC.Generics

import Board

newtype SolvedBoard = SolvedBoard [[Tile]] deriving (Eq, Generic)

instance Show SolvedBoard where
  show (SolvedBoard solvedBoard) = concatMap (\x -> show x ++ "\n") solvedBoard

instance ToJSON SolvedBoard

type UpdateFunction = (Board -> [Coordinate] -> Board)
type Rule = (Board -> Coordinate -> Bool)

solve :: BoardCharacteristics -> SolvedBoard
solve = transformToSolvedBoard . solveBoard . createBoard

solveBoard :: Board -> Board
solveBoard = applyRulesUntilBoardIsSolved . applyOneOffRules

applyOneOffRules :: Board -> Board
applyOneOffRules = convertUnsolvedTilesNotNextToTreesToGrass . convertUnsolvedTilesInTentlessLinesToGrass

convertUnsolvedTilesInTentlessLinesToGrass :: Board -> Board
convertUnsolvedTilesInTentlessLinesToGrass = applyRule isInColumnOrRowWithoutTents updateGrassTiles

applyRule :: Rule -> UpdateFunction -> Board -> Board
applyRule rule updateFunction board = updateFunction board solvedTiles
  where
    solvedTiles = filter (rule board) (boardUnsolvedTiles board)

isInColumnOrRowWithoutTents :: Rule
isInColumnOrRowWithoutTents board (x, y) =
  boardTentsPerColumn board !! x == 0 || boardTentsPerRow board !! y == 0

updateGrassTiles :: UpdateFunction
updateGrassTiles board grassTiles = board
  { boardUnsolvedTiles = filter (\coordinate -> not $ elem coordinate grassTiles) $ boardUnsolvedTiles board
  , boardSolvedTiles = boardSolvedTiles board ++ (map (\coordinate -> (coordinate, Grass)) grassTiles)
  }

updateTentTiles :: UpdateFunction
updateTentTiles board tents = board
  { boardUnsolvedTiles = filter (\coordinate -> not $ elem coordinate tents) $ boardUnsolvedTiles board
  , boardSolvedTiles = boardSolvedTiles board ++ (map (\coordinate -> (coordinate, Tent)) tents)
  }

convertUnsolvedTilesNotNextToTreesToGrass :: Board -> Board
convertUnsolvedTilesNotNextToTreesToGrass = applyRule isNotNextToAnyTrees updateGrassTiles

isNotNextToAnyTrees :: Rule
isNotNextToAnyTrees board tile = not $ any (isNormalTo tile) $ boardTrees board

isNormalTo :: Coordinate -> Coordinate -> Bool
isNormalTo t1 t2 = isToLeftOrRightOfTile t1 t2 || isToTopOrBottomOfTile t1 t2

isToLeftOrRightOfTile :: Coordinate -> Coordinate -> Bool
isToLeftOrRightOfTile (x, y) (x', y') = (x == x'-1 && y == y') || (x == x'+1 && y == y')

isToTopOrBottomOfTile :: Coordinate -> Coordinate -> Bool
isToTopOrBottomOfTile (x, y) (x', y') = (x == x' && y == y'-1) ||(x == x' && y == y'+1)

applyRulesUntilBoardIsSolved :: Board -> Board
applyRulesUntilBoardIsSolved board = applyRulesUntilBoardIsSolved' board 10
  where
    applyRulesUntilBoardIsSolved' board n = if null (boardUnsolvedTiles board) || n == 0
      then board
      else applyRulesUntilBoardIsSolved' (applyAllRules board) (n-1)

applyRules :: [Board -> Board] -> Board -> Board
applyRules rules board = foldr ($) board rules

applyAllRules :: Board -> Board
applyAllRules = applyRules
  [ fillGrassInFinishedLines
  , fillGrassNextToPlacedTents
  , fillGrassNextToSolvedTrees
  , setTentsNextToUnsolvedTreesWithOneFreeTile
  , placeTentsBasedOnUnsolvedOtherGroups
  , placeGrassBasedOnOverlappingPossibleTents
  ]

isInSameColumn :: Coordinate -> Coordinate -> Bool
isInSameColumn (x, _) (x', _) = x == x'

isInSameRow :: Coordinate -> Coordinate -> Bool
isInSameRow (_, y) (_, y') = y == y'

fillGrassInFinishedLines :: Board -> Board
fillGrassInFinishedLines
  = applyRule allTentsAreAlreadyPlacedInRow updateGrassTiles
  . applyRule allTentsAreAlreadyPlacedInColumn updateGrassTiles

allTentsAreAlreadyPlacedInRow :: Rule
allTentsAreAlreadyPlacedInRow board tile@(_, y) = length tentsInRow == requiredTentsInRow
  where
    tentsInRow = filter (isInSameRow tile) $ boardTents board
    requiredTentsInRow = boardTentsPerRow board !! y

allTentsAreAlreadyPlacedInColumn :: Rule
allTentsAreAlreadyPlacedInColumn board tile@(x, _) = length tentsInColumn == requiredTentsInColumn
  where
    tentsInColumn = filter (isInSameColumn tile) $ boardTents board
    requiredTentsInColumn = boardTentsPerColumn board !! x

fillGrassNextToPlacedTents :: Board -> Board
fillGrassNextToPlacedTents = applyRule isNextToAnyTents updateGrassTiles

isNextToAnyTents :: Rule
isNextToAnyTents board tile
  =  any (isNormalTo tile) (boardTents board)
  || any (isDiagonalTo tile) (boardTents board)

isDiagonalTo :: Coordinate -> Coordinate -> Bool
isDiagonalTo (x, y) (x', y')
  =  (x == x'-1 && y == y'-1)
  || (x == x'-1 && y == y'+1)
  || (x == x'+1 && y == y'+1)
  || (x == x'+1 && y == y'-1)

fillGrassNextToSolvedTrees :: Board -> Board
fillGrassNextToSolvedTrees = applyRule isNextToOnlySolvedTrees updateGrassTiles

isNextToOnlySolvedTrees :: Rule
isNextToOnlySolvedTrees board tile = case mapM (isSolvedTree board) trees of
  Nothing -> False
  Just b  -> getAll $ mconcat $ map All b
  where
    trees = filter (isNormalTo tile) $ boardTrees board

isSolvedTree :: Board -> Coordinate -> Maybe Bool
isSolvedTree board tree = case countChainedTentsAndTrees board Tree tree of
  Nothing             -> Nothing
  Just (tents, trees) -> Just $ tents == trees

countChainedTentsAndTrees :: Board -> Tile -> Coordinate -> Maybe (Int, Int)
countChainedTentsAndTrees board tile coordinate = countChainedTentsAndTrees' board tile coordinate coordinate
countChainedTentsAndTrees' board tile parent coordinate = add countForTile $ foldr add (Just (0,0)) countForNeighbours
  where
    countForNeighbours = map (countChainedTentsAndTrees' board otherTile coordinate) neighbours
    countForTile
      | tile == Tent = Just (1,0)
      | parent == coordinate = Just (0,1)
      | hasNoEmptyNeighbours = Just (0,1)
      | otherwise = Nothing
    otherTile = if tile == Tent then Tree else Tent
    neighbours = filter (/= parent) $ filter (isNormalTo coordinate) $ otherTiles board
    otherTiles = if tile == Tent then boardTrees else boardTents
    hasNoEmptyNeighbours = null $ filter (isNormalTo coordinate) $ boardUnsolvedTiles board
    add Nothing _                   = Nothing
    add _ Nothing                   = Nothing
    add (Just (x,y)) (Just (x',y')) = Just (x+x', y+y')

setTentsNextToUnsolvedTreesWithOneFreeTile :: Board -> Board
setTentsNextToUnsolvedTreesWithOneFreeTile = applyRule isOnlyPossibleTentNextToUnsolvedTree updateTentTiles

isOnlyPossibleTentNextToUnsolvedTree :: Rule
isOnlyPossibleTentNextToUnsolvedTree board tile = isNextToOneTree && isUnsolvedTree && treeOnlyMissesOneTile
  where
    trees = filter (isNormalTo tile) $ boardTrees board
    isNextToOneTree = length trees == 1
    isUnsolvedTree = case isSolvedTree board $ head trees of
      Nothing -> False
      Just b  -> not b
    treeOnlyMissesOneTile = length (filter (isNormalTo $ head trees) $ boardUnsolvedTiles board) == 1

placeTentsBasedOnUnsolvedOtherGroups :: Board -> Board
placeTentsBasedOnUnsolvedOtherGroups
  = applyRule isTentBasedOnUnsolvedOtherGroupsInColumn updateTentTiles
  . applyRule isTentBasedOnUnsolvedOtherGroupsInRow updateTentTiles

isTentBasedOnUnsolvedOtherGroupsInColumn :: Rule
isTentBasedOnUnsolvedOtherGroupsInColumn board tile@(x, _) = hasNoUnsolvedNeighbours && isTent
  where
    hasNoUnsolvedNeighbours = null $ filter (isToTopOrBottomOfTile tile) $ boardUnsolvedTiles board
    isTent = requiredTentsInColumn - alreadyPlacedTentsInColum == tentsPossible
    requiredTentsInColumn = boardTentsPerColumn board !! x
    alreadyPlacedTentsInColum = length $ filter (isInSameColumn tile) $ boardTents board
    tentsPossible = sum $ map possibleTentsPerGroup $ groupUnsolvedTilesInColumn board tile

groupUnsolvedTilesInColumn :: Board -> Coordinate -> [[Coordinate]]
groupUnsolvedTilesInColumn board (x, _) = foldr f [[]] [(x, y) | y <- [0..boardHeight board-1]]
  where
    f :: Coordinate -> [[Coordinate]] -> [[Coordinate]]
    f tile groups@(g:gs)
      | elem tile $ boardUnsolvedTiles board = (tile:g):gs
      | null g = groups
      | otherwise = []:groups

isTentBasedOnUnsolvedOtherGroupsInRow :: Rule
isTentBasedOnUnsolvedOtherGroupsInRow board tile@(_, y) = hasNoUnsolvedNeighbours && isTent
  where
    hasNoUnsolvedNeighbours = null $ filter (isToLeftOrRightOfTile tile) $ boardUnsolvedTiles board
    isTent = requiredTentsInRow - alreadyPlacedTentsInColum == tentsPossible
    requiredTentsInRow = boardTentsPerRow board !! y
    alreadyPlacedTentsInColum = length $ filter (isInSameRow tile) $ boardTents board
    tentsPossible = sum $ map possibleTentsPerGroup $ groupUnsolvedTilesInRow board tile

groupUnsolvedTilesInRow :: Board -> Coordinate -> [[Coordinate]]
groupUnsolvedTilesInRow board (_, y) = foldr f [[]] [(x, y) | x <- [0..boardWidth board-1]]
  where
    f :: Coordinate -> [[Coordinate]] -> [[Coordinate]]
    f tile groups@(g:gs)
      | elem tile $ boardUnsolvedTiles board = (tile:g):gs
      | null g = groups
      | otherwise = []:groups

possibleTentsPerGroup :: [Coordinate] -> Int
possibleTentsPerGroup group = ceiling $ fromIntegral (length group) / 2

placeGrassBasedOnOverlappingPossibleTents :: Board -> Board
placeGrassBasedOnOverlappingPossibleTents
   = applyRule isGrassBasedOnOverlappingTentsBelow updateGrassTiles
   . applyRule isGrassBasedOnOverlappingTentsAbove updateGrassTiles

isGrassBasedOnOverlappingTentsAbove :: Rule
isGrassBasedOnOverlappingTentsAbove board tile@(_,y) = isGrassBasedOnOverlappingTentsInRow (y-1) board tile

isGrassBasedOnOverlappingTentsBelow :: Rule
isGrassBasedOnOverlappingTentsBelow board tile@(_,y) = isGrassBasedOnOverlappingTentsInRow (y+1) board tile

isGrassBasedOnOverlappingTentsInRow :: Int -> Rule
isGrassBasedOnOverlappingTentsInRow row board tile@(x,_) = isOverlappedByTwoUnsolvedTiles && oneOfThemMustBeTent
  where
    isOverlappedByTwoUnsolvedTiles = elem (x-1, row) unsolvedTiles && elem (x+1, row) unsolvedTiles
    oneOfThemMustBeTent = requiredTentsInRow - alreadyPlacedTentsInRow - tentsPossible == (-1)
    requiredTentsInRow = boardTentsPerRow board !! row
    alreadyPlacedTentsInRow = length $ filter (isInSameRow (x,row)) $ boardTents board
    tentsPossible = length $ filter (isInSameRow (x, row)) unsolvedTiles
    unsolvedTiles = boardUnsolvedTiles board

transformToSolvedBoard :: Board -> SolvedBoard
transformToSolvedBoard board = SolvedBoard $ map mapRow [0..(boardHeight board - 1)]
  where
    mapRow y = map (findTile board) [(x,y) | x <- [0..(boardWidth board - 1)]]

findTile :: Board -> Coordinate -> Tile
findTile board coordinate = fromMaybe Empty tile
  where
    tile = findTile Grass boardGrassTiles <|> findTile Tree boardTrees <|> findTile Tent boardTents
    findTile tile collection = fmap (const tile) $ find (==coordinate) $ collection board

